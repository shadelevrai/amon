# Amon
Projet de réseau social 


## Back
Généré avec npx create-nodejs-express-app <project-name>
(https://www.npmjs.com/package/create-nodejs-express-app)

### Helmet
Helmet sert à sécuriser le serveur node/express avec plusieurs middlewares
(https://helmetjs.github.io/)
### Xss-clean
Xss-clean sert à nettoyer les infos qui arrivent en GET, POST et les parametres url
### MongoSanitize
Évite les injection de code dans la base de donnée
### Compression
Permet d'utiliser le format de compression gzip
### Cors
Permet des échanges de données entre ip différents
### Passport
Middleware qui permet la creation de compte et son utilisation
### HttpStatus
Permet d'envoyer un HTTP code et son message (ex : 404 not found) 
### Morgan
Log pour serveur node
### authLimiter
Permet de limiter les appels API par IP


## Front
Généré avec Nextjs
(https://nextjs.org)
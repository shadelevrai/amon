export default interface Error {
    error: {
        message: string,
        code?: string
    }
}
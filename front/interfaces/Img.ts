export default interface Img {
    _id?: string
    name: string | null,
    lastModified: number | null,
    // webkitRelativePath: string | null,
    size: number | null,
    type: string | null
    base64: string | null | ArrayBuffer
}
import BirthDay from "./BirthDay";

export default interface User {
    user?: {
      _id?: string;
      email: string;
      lastName:string,
      firstName:string,
      birthDay:BirthDay,
      token?: string,
      articles:[string],
      image: {
        imgProfile:string,
        imgCover? : string,
        imgAlbum? : Array<string>
      }
    };
    errors?: {
      email?: string;
      password?: string;
      message?:string;
    };
    errorServer?: string;
  }
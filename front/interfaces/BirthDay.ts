export default interface BirthDay {
    day: number | null | string,
    month: number | null | string,
    year: number | null | string
}
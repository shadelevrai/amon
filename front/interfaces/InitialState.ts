import BirthDay from "./BirthDay";

export default interface InitialState {
  inputTxtEmailCreateAccount: string;
  inputTxTPasswordCreateAccount: string;
  isLogging: boolean,
  user: {
    token?: string,
    email: string;
    lastName: string,
    firstName: string,
    birthday: BirthDay,
    image: {
      imgProfile: { base64: string },
      imgCover: string | null,
      imgAlbum: Array<string>
    },
    articles:[string]
  }
}


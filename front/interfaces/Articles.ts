export default interface Articles {
    _id: string,
    text: string,
    date: Date
}
import InitialState from "./interfaces/InitialState"

const initialState: InitialState = {
  inputTxtEmailCreateAccount: "",
  inputTxTPasswordCreateAccount: "",
  isLogging: false,
  user: {
    email: "",
    lastName: "",
    firstName: "",
    token:"",
    birthday: {
      day: null,
      month: null,
      year: null
    },
    image:{
      imgProfile:"",
      imgCover :"",
      imgAlbum : []
    },
    articles:[]
  }
};

function reducer(state: any = initialState, action: any): InitialState {
  switch (action.type) {
    case "INPUT_TXT_EMAIL_CREATE_ACCOUNT":
      return {
        ...state,
        inputTxtEmailCreateAccount: action.step,
      };
    case "INPUT_TXT_PASSWORD_CREATE_ACCOUNT":
      return {
        ...state,
        inputTxTPasswordCreateAccount: action.step,
      };
    case "USER":
      return {
        ...state,
        user: action.step
      }
    case "INPUT_TXT_EMAIL_LOGIN_ACCOUNT":
      return {
        ...state,
        inputTxtEmailLoginAccount: action.step
      }
    case "INPUT_TXT_PASSWORD_LOGIN_ACCOUNT":
      return {
        ...state,
        inputTxtPasswordLoginAccount: action.step
      }
    default:
      return state;
  }
}

export default reducer;

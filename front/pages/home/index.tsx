import { useSelector, useDispatch, useStore } from "react-redux";
import { useEffect, useState, Fragment } from "react";
import type { NextPage } from "next";

import Disconnect from "../../components/disconnect";

import getUserServerSide from "../../libs/getUserServerSide";
import { getImageAPI } from "../../libs/API/image";

import User from "../../interfaces/User";
import InitialState from "../../interfaces/InitialState";
import Img from "../../interfaces/Img";
import Error from "../../interfaces/Error";
import AddPost from "../../components/AddPost";

import ShowMyPost from "../../components/ShowMyPost";
import { getArticlesAPI } from "../../libs/API/article";

const Home: NextPage<User> = ({ user }) => {
    const dispatch = useDispatch();
    const reduxState = useSelector((state: InitialState) => state);

    useEffect(() => {
        dispatch({ type: "USER", step: user })
    }, [])

    return (
        <Fragment>
            <AddPost />
            {reduxState.user.articles.length}
            <ShowMyPost />
            <Disconnect />
        </Fragment>
    )
}

export async function getServerSideProps(ctx: any) {
    const responseUser: User | null | false = await getUserServerSide(ctx)

    if (!responseUser) {
        ctx.res.writeHead(301, {
            location: "/",
            'Content-Type': 'text/html; charset=utf-8',
        }), ctx.res.end()
    }

    //get the image profile
    const responseImgProfile: Promise<Img | Error> = await getImageAPI(responseUser.user.image.imgProfile, "images/", responseUser.user.token)
    !responseImgProfile.error && (responseUser.user.image.imgProfile = responseImgProfile)

    //get the articles object
    responseUser.user.articles = await Promise.all(
        responseUser.user.articles.map(async (article) => {
            const res = await getArticlesAPI("articles/", responseUser.user.token, article)
            return res.article
        })
    )

    return {
        props: {
            user: responseUser.user,
        }
    }
}

export default Home
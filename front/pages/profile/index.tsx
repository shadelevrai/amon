import { FC, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import { updateUserImageCoverAPI, removeUserImageCoverAPI } from "../../libs/API/user";
import getUserServerSide from "../../libs/getUserServerSide";
import { deleteImageAPI, getImageAPI, uploadImageAPI } from "../../libs/API/image";

import User from "../../interfaces/User";
import InitialState from "../../interfaces/InitialState";
import Img from "../../interfaces/Img";
import Error from "../../interfaces/Error"

import UploadImage from "../../components/UploadImage";

const Profile: FC<User> = ({ user }) => {

    const dispatch = useDispatch();
    const reduxState = useSelector((state: InitialState) => state);

    const [selectedImgProfil, setSelectedImgProfil] = useState<Img>({
        name: null,
        lastModified: null,
        size: null,
        type: null,
        base64: null
    })

    const [imgCoverBase64FromLocalStorage, setImgCoverBase64FromLocalStorage] = useState<String | null>("")

    useEffect(() => {

        (async function () {
            const sessionStorageImgCover = sessionStorage.getItem("imgCover")
            //Si y'a une image cover dans le session storage
            if (sessionStorageImgCover) {
                setImgCoverBase64FromLocalStorage(sessionStorageImgCover)
            } else {
                //Si y'a une image cover dans l'user (sans base64) mais pas de image cover dans le session storage
                const responseImgCoverBase64: Img | null | Error = (user?.image.imgCover?._id && user.token) ? await getImageAPI(user.image.imgCover._id, "images/", user.token) : null
                if (responseImgCoverBase64?.name) {
                    sessionStorage.setItem("imgCover", responseImgCoverBase64.base64)
                    setImgCoverBase64FromLocalStorage(responseImgCoverBase64.base64)
                }
            }
        })()
        dispatch({ type: "USER", step: user })
    }, [])

    //Pour upload une image cover et la mettre dans l'user
    useEffect(() => {
        (async function () {
            if (selectedImgProfil.name) {
                //upload de l'id de l'image dans l'user database
                const resImage: Img | Error = await uploadImageAPI(selectedImgProfil, "images/upload-image/")
                const resUser: User | Error | null = (resImage._id && reduxState.user.token) ? await updateUserImageCoverAPI(resImage._id, reduxState.user.token) : null
                if (resUser?._id) {
                    dispatch({ type: "USER", step: resUser })
                    //récupération du base64 de l'image cover de l'utilisateur et stockage dans le localstorage
                    const responseImgCoverBase64: Img | Error = await getImageAPI(resUser.image.imgCover, "images/", resUser.token)
                    responseImgCoverBase64.name && sessionStorage.setItem("imgCover", responseImgCoverBase64.base64)
                    responseImgCoverBase64.name && setImgCoverBase64FromLocalStorage(responseImgCoverBase64.base64)
                }
            }
        })()
    }, [selectedImgProfil])

    async function deleteImageCover() {
        sessionStorage.removeItem("imgCover")
        setImgCoverBase64FromLocalStorage("")
        const resUser: User | Error = await removeUserImageCoverAPI(reduxState.user.token)
        if (!resUser.error) {
            const resImageDeleted = await deleteImageAPI(reduxState.user.image.imgCover._id, "images/delete-image", reduxState.user.token)
            !resImageDeleted.error && dispatch({ type: "USER", step: resUser })
        }
    }

    return (
        <>
            <div className="block profile">
                <div className="columns">
                    {reduxState.user.image.imgCover ?
                        <div className="column is-full">
                            <button className="delete button-delete-img-profile" onClick={() => deleteImageCover()}></button>
                            <figure className="image is-2by1">
                                <img src={`${imgCoverBase64FromLocalStorage}`} />
                            </figure>
                        </div>
                        :
                        <div className="column is-full has-background-light">
                            <div className="level">
                                <div className="level-left">
                                    <p>Vous n'avez pas de photo de couverture</p>
                                </div>
                                <div className="level-right">
                                    <UploadImage setSelectedImgProfil={setSelectedImgProfil} selectedImgProfil={selectedImgProfil} showImageSelected={false} preview={false} />
                                </div>
                            </div>
                        </div>}
                </div>
            </div>
        </>
    )
}

export async function getServerSideProps(ctx: any) {
    const responseUser = await getUserServerSide(ctx)

    if (!responseUser) {
        ctx.res.writeHead(301, {
            location: "/",
            'Content-Type': 'text/html; charset=utf-8',
        }), ctx.res.end()
    }


    const responseImgProfile: Promise<Img | Error> = await getImageAPI(responseUser.user.image.imgProfile, "images/", responseUser.user.token)
    !responseImgProfile.error && (responseUser.user.image.imgProfile = responseImgProfile)

    const responseImgCover: Promise<Img> = responseUser.user.image.imgCover ? await getImageAPI(responseUser.user.image.imgCover, "images/", responseUser.user.token) : { error: { message: "pas d'image cover" } }

    !responseImgCover.error && (responseUser.user.image.imgCover = responseImgCover)

    return {
        props: {
            user: responseUser && responseUser.user
        }
    }
}

export default Profile
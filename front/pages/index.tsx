import type { NextPage } from "next";
import { useRouter } from "next/router";
import Link from "next/link";
import CreateAndLogin from "../components/CreateAndLogin";

import getUserServerSide from "../libs/getUserServerSide";

const Home: NextPage = () => {
  const router = useRouter()
  return (
    <div className="p-6 index">
      <h1 className="title has-text-centered is-lowercase is-italic has-text-weight-bold">Amon</h1>
      <div className="homeBoxLogin p-5 box">
        <CreateAndLogin step="login" />
      </div>
      <nav>
        <div className="level">
          <div className="level-left"></div>
          <div className="level-right">
            <div className="level-item">
              <Link href="/sign-up">
                <a>S&apos;enregistrer</a>
              </Link>
            </div>
          </div>
        </div>
        <p onClick={()=>router.push("/reset-password")}>
          <a>Vous avez oublié votre mot de passe ?</a>
        </p>
      </nav>
    </div>
  );
};

export async function getServerSideProps(ctx: any) {
  const user = await getUserServerSide(ctx)

  user &&
    (ctx.res.writeHead(301, {
      location: "/home",
      'Content-Type': 'text/html; charset=utf-8',
    }),
      ctx.res.end())

  return {
    props: {
      test: "lala"
    }
  }
}

export default Home;

import { NextPage } from "next";
import { getCheckIfEmailExist } from "../../libs/API/user";

const resetPassword: NextPage = () => {

    let email: string = "aaaaaa@aaaaaa.fr"

    function handleMail(e: any): void {
        email = e.target.value
    }

    async function checkIfMailExist(): Promise<void> {
        const res = await getCheckIfEmailExist(email)
        console.log("🚀 ~ file: index.tsx ~ line 14 ~ checkIfMailExist ~ res", res)
    }

    return (
        <>
            <p>Quelle est votre email?</p>
            <input type="text" onChange={e => handleMail(e)} />
            <input type="submit" onClick={() => checkIfMailExist()} value="Recevoir un email" />
        </>
    )
}

export default resetPassword
import { useEffect, useState } from "react"
import Tata from "../../components/Tata"

const Testb = () => {

    const [test, setTest] = useState({ lala: "lala", lolo: "lolo", lili: "lili" })

    useEffect(() => {
        console.log("🚀 ~ file: index.tsx ~ line 6 ~ Testbb ~ test", test)
    }, [test])

    return (
        <Tata setTest={setTest}/>
    )
}

export default Testb
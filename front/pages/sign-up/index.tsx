import type { NextPage } from "next";
import CreateUser from "../../components/CreateUser";


const SignUp: NextPage = () => {
  return (
    <div>
      <h1 className="title has-text-centered is-lowercase is-italic has-text-weight-bold">S&apos;enregistrer</h1>
      <div className="homeBoxLogin p-5 box">
        <CreateUser/>
      </div>
    </div>
  );
};

export default SignUp;

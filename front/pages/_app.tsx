import { Provider } from "react-redux";
import { createStore } from "redux";
import { useRouter } from 'next/router'
import reducer from "../store";
import type { AppProps /*, AppContext */ } from 'next/app'

import NavBar from "../components/NavBar";

import "bulma"
import "../styles/global.scss"

const store = createStore(reducer);

function MyApp({ Component, pageProps, test }: AppProps) {
  const router = useRouter()

  if (router.pathname === "/") {
    return (
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    )
  }

  if (router.pathname === "/reset-password") {
    return (
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    )
  }

  if (router.pathname === "/sign-up") {
    return (
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    )
  }

  return (
    <Provider store={store}>
      <NavBar />
      <Component {...pageProps} />
    </Provider>
  );
}

MyApp.getInitialProps = async ({ ctx }: any) => {

  // if (typeof window === "undefined") {
  //   const allCookieString = ctx.req.headers.cookie ? ctx.req.headers.cookie : null
  //   const token: string | null = allCookieString ? cookie.parse(ctx.req.headers.cookie).user : null
  //   const responseUser: ResponseDataUser | false | null = token ? await checkValidityToken(token) : null

  //   if (responseUser === null && (ctx.pathname === "/home" || ctx.pathname === "/test")) {
  //     ctx.res.writeHead(301, {
  //       location: "/",
  //       'Content-Type': 'text/html; charset=utf-8',
  //     })
  //     ctx.res.end()
  //   }
  //   if (responseUser && ctx.pathname === "/") {
  //     ctx.res.writeHead(301, {
  //       location: "/home",
  //       'Content-Type': 'text/html; charset=utf-8',
  //     })
  //     ctx.res.end()
  //   }
  // }


  // }
  // const env = process.env.NODE_ENV
  // if (env == "development") {
  //   // do something
  //   console.log("🚀 ~ file: _app.tsx ~ line 31 ~ MyApp.getInitialProps= ~ env", env)
  // }
  // else if (env == "production") {
  //   // do something
  // }
  // const appProps = await App.getInitialProps(appContext);
  // console.log("🚀 ~ file: _app.tsx ~ line 59 ~ MyApp.getInitialProps= ~ appProps", appProps.req)
  return { test: "lala" };
};

export default MyApp;

import type { NextPage } from "next"
import { useEffect, useState } from "react"

const Test: NextPage = () => {

  let mailInscription = "lalala@lalala.com"
  let passwordInscription = "lalala"

  let mailConnection = "lalala@lalala.com"
  let passwordConnection = "lalala"

  interface User {
    mail: String | null,
    _id: String | null,
    point: Number | null
  }

  const [user, setUser] = useState<User>({
    mail: null,
    _id: null,
    point: null
  })
  const [messageResponsePointServer, setMessageResponsePointServer] = useState(false)

  useEffect(() => {
    console.log("🚀 ~ file: index.tsx ~ line 14 ~ user", user.point)
  }, [user])

  function handleMailInscription(value) {
    mailInscription = value
  }

  function handlePasswordInscription(value) {
    passwordInscription = value
  }

  function handleMailConnection(value) {
    mailConnection = value
  }

  function handlePasswordConnection(value) {
    passwordConnection = value
  }

  async function inscription(): Promise<void> {
    try {
      // const data = { mail: mail, password: password }
      const data = { mailInscription, passwordInscription }
      const resDataRaw = await fetch("http://localhost:8888/envoi-data", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(data)
      })
      const resData = await resDataRaw.json()
      resData.user._id && setUser(resData.user)
    } catch (error) {
    }

  }

  async function connection(): Promise<void> {
    try {
      const data = { mailConnection, passwordConnection }
      const resDataRaw = await fetch("http://localhost:8888/logging", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(data)
      })
      const { user } = await resDataRaw.json()
      console.log("🚀 ~ file: index.tsx ~ line 59 ~ connection ~ resData", user)
      user._id && setUser(user)
    } catch (error) {

    }
  }

  function addPoint(): void {
    setUser(lala => ({
      ...lala,
      point: lala.point + 1,
    }))
  }

  async function sendPointsServer(): Promise<void> {
    try {
      const data = { id: user._id, point: user.point }
      const resDataRaw = await fetch("http://localhost:8888/upgrade", {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(data)
      })
      const resData = await resDataRaw.json()
      resData.message === "Les points ont été modifié" && setMessageResponsePointServer(true)
    } catch (error) {
      console.log("🚀 ~ file: index.tsx ~ line 93 ~ sendPointsServer ~ error", error)
    }
  }

  function logout() {
    setUser({
      mail: null,
      _id: null,
      point: null
    })
  }

  return (
    <>
      {user._id ? <>
        <p> bonjour {user.mail} </p>
        <p> vous avez {user.point} point(s) </p>
        <button onClick={() => addPoint()}>Agmenter les points</button>
        <button onClick={() => sendPointsServer()}>Envoyer les points au serveur</button>
        {messageResponsePointServer && <p> Vous avez bien mis à jour les points dans la base de donnée </p>}
        <button onClick={() => logout()}>Se déconnecter</button>
      </> : <>
        <h1>Créer un compte</h1>
        <label htmlFor="mail">mail</label>
        <input type="text" onChange={(e) => handleMailInscription(e.target.value)} />

        <label htmlFor="">password</label>
        <input type="password" onChange={(e) => handlePasswordInscription(e.target.value)} />

        <button onClick={() => inscription()}>inscription</button>

        <h1>Se connecter</h1>

        <label htmlFor="mail">mail</label>
        <input type="text" onChange={(e) => handleMailConnection(e.target.value)} />

        <label htmlFor="">password</label>
        <input type="password" onChange={(e) => handlePasswordConnection(e.target.value)} />

        <button onClick={() => connection()}>inscription</button>
      </>
      }
    </>
  )
}

export default Test
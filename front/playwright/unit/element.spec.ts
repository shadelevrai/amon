import { test, expect } from "@playwright/test";

test.describe("it should have a title", () => {
  test("/", async ({ page }) => {
    await page.goto("/");
    const title = page.locator("h1");
    await expect(title).toHaveText("Amon");
  });
});

import { test, expect } from "@playwright/test";

test("should navigate to the signup page", async ({ page }) => {
  await page.goto("/");
  await page.click("text=S'enregistrer");
  await expect(page).toHaveURL("/sign-up");
  await expect(page.locator("h1")).toContainText("S'enregistrer");
});

import { test, expect, Page } from "@playwright/test";

import { env } from "../../next.config";

import BirthDay from "../../interfaces/BirthDay"
import img from "../../interfaces/Img"

import base64 from "./base64"

interface Body {
    user: {
        _id: string;
        email: string;
        token: string;
        lastName: string,
        firstName: string,
        birthDay: BirthDay
    };
}

const generateString = (): string => Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

const generateEmail = (): string => generateString() + "@playwright.test"

const generatePassword = (): string => generateString()

function userData(body: Body) {
    const { _id, email, lastName, firstName, birthDay, token } = body.user
    const user = {
        user: {
            _id,
            email,
            lastName,
            firstName,
            token,
            birthDay
        }
    }
    return user
}

const imgWihtoutBase64 = {
    _id: "6213dd10fb242b091cb855f8",
    name: "image-name",
    lastModified: 1644081567249,
    size: 12537,
    type: "image/jpeg",
}

const userObj = {
    user: {
        email: generateEmail(),
        password: generatePassword(),
        lastName: generateString(),
        firstName: generateString(),
        birthDay: {
            day: 15,
            month: 10,
            year: 1990
        },
        image: {
            imgProfile: imgWihtoutBase64._id
        }
    }
};

let token: string = ""

const postApiUser = async (page: Page, link: string, token: string | null, method: string, emailChange: object | null): Promise<object> => {
    const headers: any = token ? {
        "Content-Type": "application/json",
        Authorization: `Token ${token}`,
    } : {
        "Content-Type": "application/json"
    }

    switch (method) {
        case "post":
            return page.request.post(`${env?.HOST_SERVER}/api/${link}`, {
                headers,
                data: userObj,
            });
        case "get":
            return page.request.get(`${env?.HOST_SERVER}/api/${link}`, {
                headers,
            });
        case "patch":
            return page.request.patch(`${env?.HOST_SERVER}/api/${link}`, {
                headers,
                data: emailChange,
            });
        case "delete":
            return page.request.delete(`${env?.HOST_SERVER}/api/${link}`, {
                headers,
            });
        default:
            return { message: "error" };
    }
}

const postApiImage = async (page: Page, link: string): Promise<object> => {
    return page.request.post(`${env?.HOST_SERVER}/api/${link}`, {
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify({ imgWihtoutBase64, base64 })
    })
}


test("Can create an user in my database", async ({ page }): Promise<void> => {
    const responseCreate: any = await postApiUser(page, "users/", null, "post", null)
    expect(responseCreate.ok()).toBeTruthy()
    const body: Body = await responseCreate.json();
    expect(body).toMatchObject(userData(body))
});

test("Cant create an user if email is taken", async ({ page }): Promise<void> => {
    const responseCreate: any = await postApiUser(page, "users/", null, "post", null)
    expect(responseCreate.ok()).toBe(false)
})


test("Can login with my account", async ({ page }): Promise<void> => {
    const responseLogin: any = await postApiUser(page, "users/login", null, "post", null)
    expect(responseLogin.ok()).toBeTruthy()
    const body: Body = await responseLogin.json();
    expect(body).toMatchObject(userData(body))
    token = body.user.token;
});

test("Can get my data with my account", async ({ page }): Promise<void> => {
    const responseGetData: any = await postApiUser(page, "users/current", token, "get", null);
    expect(responseGetData.ok()).toBeTruthy()
    const body = await responseGetData.json();
    expect(body).toMatchObject(userData(body))
});

// test("Can modify my user email in the database", async ({ page }): Promise<void> => {
//     const emailChange = { user: { email: generateEmail() } };
//     const responseModify: any = await postApiUser(page, "users/modify", token, "patch", emailChange);
//     expect(responseModify.ok()).toBeTruthy()
//     const body = await responseModify.json();
//     expect(body).toMatchObject(userData(body))
// });

test("Can delete my account in the database", async ({ page }): Promise<void> => {
    const responseDelete: any = await postApiUser(page, "users/delete", token, "delete", null);
    expect(responseDelete.status()).toBe(200);
    expect(responseDelete.ok()).toBeTruthy()
    const bodyJSON = await responseDelete.json();
    const { message } = bodyJSON;
    expect(message).toBe("User deleted");
});

test("Can ulpload an image", async ({ page }): Promise<void> => {
    const responseCreateImage: any = await postApiImage(page, "images/upload-image")
    expect(responseCreateImage.ok()).toBeTruthy()
    const body: Body = await responseCreateImage.json();
    expect(body).toMatchObject(img)
})
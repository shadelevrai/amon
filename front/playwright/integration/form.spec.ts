import { test, expect } from "@playwright/test";

test("On peut entrer du texte dans les input de la page sign-up",async({page})=>{
    await page.goto("/sign-up")
    await page.fill("[type=email]","peter@lalala.com")
    await page.fill("[type=password]","thepassword")
    await page.fill("#last_name","Prker")
    await page.fill("#first_name", "Peter")
    await page.selectOption("select#birthday_day","15")
    await page.selectOption("select#birthday_month","5")
    await page.selectOption("select#birthday_year","1990")
    await page.setInputFiles("input#input_file","./public/test/img.jpg")
    // await (await page.waitForSelector("#submit")).click()
})
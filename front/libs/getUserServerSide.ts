import cookie from "cookie"
import checkValidityToken from "./checkValidityToken"
import { getImageAPI } from "./API/user"

import type User from "../interfaces/User"

interface Ctx {
    req: {
        headers: {
            cookie: string
        }
    }
}

export default async function getUserServerSide(ctx: Ctx) {
    const allCookieString = ctx.req.headers.cookie ? ctx.req.headers.cookie : null
    const token: string | null = allCookieString ? cookie.parse(ctx.req.headers.cookie).user : null
    const responseUser: User | false | null = token ? await checkValidityToken(token) : null
    // if (!responseUser) {
    //     ctx.res.writeHead(301, {
    //         location: "/",
    //         'Content-Type': 'text/html; charset=utf-8',
    //     }), ctx.res.end()
    // }
    // if (responseUser) {
    //     const responseImg = await getImageAPI(responseUser.user.imgProfile, "images/", responseUser.user.token)
    //     responseUser.user.imgProfile = responseImg.image
    // }
    return responseUser
}
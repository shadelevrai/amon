import { getCookie } from 'cookies-next';
import { getUserData } from "./API/user"

import type User from '../interfaces/User';
import type Error from '../interfaces/Error';

export default async function checkValidityToken(token: any): Promise<User | false> { 
    const response: User|Error = await getUserData(token ? token : getCookie("user"))
    if (response.user) return response
    return false
}
import Error from "../../interfaces/Error"
import Img from "../../interfaces/Img"

async function uploadImageAPI(img: Img, link: string): Promise<Img | Error> {
    try {
        const { name, lastModified, size, type } = img
        const base64 = img.base64
        const imgWihtoutBase64 = { name, lastModified, size, type }
        const res = await fetch(`${process.env.HOST_SERVER}/api/${link}`, {
            method: "post",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ imgWihtoutBase64, base64 })
        })
        const responseData: Promise<Img | Error> = await res.json().then(result => result.image ? result.image : result)
        return responseData
    } catch (err) {
        const responseData: Error = {
            error:
            {
                message: "Mauvais lien ou serveur non atteignable",
            }
        };
        return responseData
    }
}

async function getImageAPI(id: string, link: string, token: string): Promise<Img | Error> {
    try {
        const res = await fetch(`${process.env.HOST_SERVER}/api/${link}/${id}`, {
            method: "get",
            headers: {
                "Content-Type": "application/json",
                'Authorization': 'Token ' + token
            }
        })
        const responseData: Promise<Img | Error> = await res.json().then(result => result.image ? result.image : result)
        return responseData
    } catch (error) {
        const responseData: Error = {
            error: {
                message: "Mauvais lien ou serveur non atteignable",
            }
        };
        return responseData
    }
}

async function deleteImageAPI(id: string, link: string, token: string): Promise<{ message: string } | Error> {
    try {
        const res = await fetch(`${process.env.HOST_SERVER}/api/${link}/${id}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                'Authorization': 'Token ' + token
            }
        })
        const responseData: { message: string } | Error = await res.json()
        return responseData
    } catch (error) {
        const responseData: Error = {
            error: {
                message: "Mauvais lien ou serveur non atteignable",
            }
        };
        return responseData
    }
}

export { uploadImageAPI, getImageAPI, deleteImageAPI };
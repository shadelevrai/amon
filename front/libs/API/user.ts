import User from "../../interfaces/User";
import Error from "../../interfaces/Error"

async function postAPI(dataUser: User, link: string): Promise<User | Error> {
  try {
    const res = await fetch(`${process.env.HOST_SERVER}/api/users${link}`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(dataUser),
    });
    const responseData: Promise<User | Error> = await res.json()
    return responseData;
  } catch (err) {
    const responseData: Error = {
      error: {
        message: "Mauvais lien ou serveur non atteignable",
      }
    };
    return responseData;
  }
}

async function getUserData(token: string): Promise<User | Error> {
  try {
    const res = await fetch(`${process.env.HOST_SERVER}/api/users/current`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${token}`
      }
    })
    const responseData = await res.json()
    return responseData
  } catch (error) {
    const responseData: Error = {
      error: {
        message: "Mauvais lien ou serveur non atteignable",
      }
    };
    return responseData
  }
}

async function updateUserImageCoverAPI(img_id: string | null, token: string): Promise<User | Error> {
  try {
    const res = await fetch(`${process.env.HOST_SERVER}/api/users/add-new-image-cover-for-user`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${token}`
      },
      body: JSON.stringify({ img_id }),
    })
    const responseData = await res.json().then(result => result.user ? result.user : result)
    return responseData
  } catch (error) {
    const responseData: Error = {
      error: {
        message: "Mauvais lien ou serveur non atteignable",
      }
    };
    return responseData
  }
}

async function removeUserImageCoverAPI(token: string) {
  try {
    const res = await fetch(`${process.env.HOST_SERVER}/api/users/remove-image-cover-for-user`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${token}`
      }
    })
    const responseData: User = await res.json().then(result => result.user ? result.user : result)
    return responseData
  } catch (error) {
    const responseData: Error = {
      error: {
        message: "Mauvais lien ou serveur non atteignable",
      }
    };
    return responseData
  }
}

async function addNewElementInArray(token: string, elem: string): Promise<User | Error> {
  try {
    const res = await fetch(`${process.env.HOST_SERVER}/api/users/add-element-array`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${token}`
      },
      body: JSON.stringify({ elem }),
    })
    const responseData: User = await res.json().then(result => result.user ? result.user : result)
    return responseData
  } catch (error) {
    const responseData: Error = {
      error: {
        message: "Mauvais lien ou serveur non atteignable",
      }
    };
    return responseData
  }
}

async function getCheckIfEmailExist(email: string): Promise<{ message: boolean } | Error> {
  try {
    const res = await fetch(`${process.env.HOST_SERVER}/api/users/mail-exist/${email}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      }
    })
    const responseData: { message: boolean } = await res.json()
    return responseData
  } catch (error) {
    const responseData: Error = {
      error: {
        message: "Mauvais lien ou serveur non atteignable",
      }
    };
    return responseData
  }
}

export { postAPI, getUserData, updateUserImageCoverAPI, removeUserImageCoverAPI, addNewElementInArray, getCheckIfEmailExist };

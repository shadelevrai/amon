async function postArticleAPI(text: string, link: string, token: string) {
    try {
        const article = {
            text
        }
        const res = await fetch(`${process.env.HOST_SERVER}/api/${link}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                'Authorization': 'Token ' + token
            },
            body: JSON.stringify({ article })

        })
        const responseData = await res.json().then(result => result.article ? result.article : result)
        return responseData
    } catch (error) {
        const responseData = {
            error:
            {
                message: "Mauvais lien ou serveur non atteignable",
            }
        };
        return responseData
    }
}

async function getArticlesAPI(link: string, token: string, idArticle: string) {
    try {
        const res = await fetch(`${process.env.HOST_SERVER}/api/${link}/${idArticle}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                'Authorization': 'Token ' + token
            }
        })
        const responseData = await res.json()
        return responseData
    } catch (error) {
        const responseData = {
            error:
            {
                message: "Mauvais lien ou serveur non atteignable",
            }
        };
        return responseData
    }
}

export { postArticleAPI, getArticlesAPI }
import { setCookies } from "cookies-next";

export default function putCookie(name: string, token: string): boolean {
  try {
    setCookies(name, token, { 
        sameSite: true 
    });
    return true;
  } catch (error) {
    console.log("🚀 ~ file: putCookie.ts ~ line 8 ~ putCookie ~ error", error);
    return false
  }
}

const allPages = [{
    link: "/",
    description: "Première page",
    secure: false
}
    , {
    link: "/sign-up",
    description: "S'enregistrer",
    secure: false
},{
    link: "/home",
    description: "La home page",
    secure: true
}]

export default allPages
import { useState, FC } from "react"
import Img from "../../interfaces/Img"

interface Props {
    setSelectedImgProfil: Function,
    selectedImgProfil: Img,
    showImageSelected: boolean,
    preview?: boolean
}

const UploadImage: FC<Props> = ({ setSelectedImgProfil, selectedImgProfil, showImageSelected, preview = true }) => {
    const [imageProfileSizeError, setImageProfileSizeError] = useState<boolean>(false)

    function checkImgSize(img: Img): void {
        try {
            if (img.size && img.size < 5000000) {
                const fileReader = new FileReader();
                fileReader.readAsDataURL(img)
                fileReader.onload = () => {
                    img.base64 = fileReader.result;
                    setSelectedImgProfil(img);
                    setImageProfileSizeError(false)
                }
            } else {
                setImageProfileSizeError(true)
            }
        } catch (err) {
            console.log("🚀 ~ file: index.tsx ~ line 18 ~ checkImgSize ~ err", err)
        }

    }

    return (
        <>
            <div className="file has-name">
                <label className="file-label">
                    <input className="file-input" type="file" name="file" accept="image/*" id="input_file" onChange={e => e.target.files && checkImgSize(e.target.files[0])} />
                    <span className="file-cta">
                        <span className="file-icon">
                            <i className="fas fa-upload"></i>
                        </span>
                        <span className="file-label">
                            Choisissez une image
                        </span>
                    </span>
                    {preview && <span className="file-name">
                        {selectedImgProfil?.name}
                    </span>}

                </label>
            </div>
            {preview && (showImageSelected && selectedImgProfil.name && process.env.NODE_ENV != "development" ? <figure className="image is-128x128"> <img src={URL.createObjectURL(selectedImgProfil)} /></figure> : <img className="is-rounded" src={`${selectedImgProfil.base64}`} alt="" />)}
            {imageProfileSizeError && <p>Erreur ! Votre image doit faire moins de 5 mo</p>}
        </>
    )
}

export default UploadImage
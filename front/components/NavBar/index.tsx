import { FunctionComponent, useEffect } from "react"
import Link from "next/link";
import { useSelector, useDispatch } from "react-redux";

// import { getImageAPI } from "../../libs/API";

import Disconnect from "../disconnect";

const NavBar = () => {
    const reduxState = useSelector((state: any) => state);

    return (
        <div className="block">
            <nav className="navbar" role="navigation" aria-label="main navigation">
                <div className="navbar-menu">
                    <div className="navbar-start">
                        <Link href="/home">
                            <a className="is-capitalized has-text-weight-bold is-family-code is-size-3 ml-4 has-text-black">Amon</a>
                        </Link>
                    </div>
                    <div className="navbar-end">
                        <div className="navbar-item">
                            <Link href="/profile">
                                <a>{reduxState.user.firstName}</a>
                            </Link>
                        </div>
                        <div className="navbar-item">
                            <figure className="image">
                                <img className="is-rounded" src={`${reduxState.user.image?.imgProfile.base64}`} alt="" />
                            </figure>
                        </div>
                        <div className="navbar-item">
                            <Disconnect />
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default NavBar
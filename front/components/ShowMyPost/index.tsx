import { FunctionComponent, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import InitialState from "../../interfaces/InitialState";

const ShowMyPost: FunctionComponent = () => {

    const reduxState = useSelector((state: InitialState) => state);

    const [articles, setArticles] = useState([])

    useEffect(() => {
        setArticles(reduxState.user.articles.reverse())
    }, [reduxState.user.articles])

    return articles.map((article: string) =>
        <div className="block box" key={article._id}>
            <div className="level">
                <div className="level-left">
                    <div className="level-item">
                        <figure className="image is-32x32">
                            <img className="is-rounded" src={`${reduxState.user.image?.imgProfile.base64}`} alt="" />
                        </figure>
                    </div>
                    <div className="level-item">
                        <div className="column">
                            <p> {reduxState.user.firstName} {reduxState.user.lastName}</p>
                            <div> {new Date(article.date).toUTCString()} </div>
                            <div> {article.text} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default ShowMyPost
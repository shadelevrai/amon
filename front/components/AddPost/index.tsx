import { FunctionComponent, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Articles from "../../interfaces/Articles";
import Error from "../../interfaces/Error";
import Img from "../../interfaces/Img";
import InitialState from "../../interfaces/InitialState";
import User from "../../interfaces/User";
import { postArticleAPI } from "../../libs/API/article";
import { uploadImageAPI } from "../../libs/API/image";
import { addNewElementInArray } from "../../libs/API/user";
import UploadImage from "../UploadImage";

const AddPost: FunctionComponent = () => {
    const reduxState = useSelector((state: InitialState) => state);
    const dispatch = useDispatch();

    const [textArticle, setTextArticle] = useState<string>("")
    const [selectedImgArticle, setSelectedImgArticle] = useState<Img>({
        name: null,
        lastModified: null,
        size: null,
        type: null,
        base64: null
    })

    function handleTexte(text: string): void {
        textArticle.length < 100 && setTextArticle(text)
    }

    function removeSelectedImg(): void {
        setSelectedImgArticle({
            name: null,
            lastModified: null,
            size: null,
            type: null,
            base64: null
        })
    }

    async function sendPost(): Promise<void> {

        async function uploadAnArticleText() {
            const resTextArticle: Articles | Error = await postArticleAPI(textArticle, "articles/post-article", reduxState.user.token)
            if (!resTextArticle.error) {
                const resNewElementArray: User | Error = await addNewElementInArray(reduxState.user.token, resTextArticle._id)
                return {resNewElementArray,resTextArticle}
            }
        }

        async function uploadAnArticleImage() {
            const resImgArticle: Img | Error = await uploadImageAPI(selectedImgArticle, "images/upload-image")
            if (!resImgArticle.error) {
                const resNewElementArray: User | Error = await addNewElementInArray(reduxState.user.token, resImgArticle._id)
                console.log("🚀 ~ file: index.tsx ~ line 55 ~ uploadAnArticleImage ~ resNewElementArray", resNewElementArray)
            }
        }

        try {
            if (textArticle && !selectedImgArticle.name) {
                const res = await uploadAnArticleText()
                if (res.resTextArticle._id) {
                    console.log("🚀 ~ file: index.tsx ~ line 61 ~ sendPost ~ res", res)
                    const arr = reduxState.user.articles
                    arr.push(res?.resTextArticle)
                    const use = reduxState.user
                    use.articles = arr
                    dispatch({ type: "USER", step: use })
                }
            }

            if (!textArticle && selectedImgArticle.name) {
                uploadAnArticleImage()
            }

            if (textArticle && selectedImgArticle.name) {
                uploadAnArticleText()
                uploadAnArticleImage()
            }
        } catch (error) {
            console.log("🚀 ~ file: index.tsx ~ line 39 ~ sendPost ~ error", error)
        }
    }

    return (
        <div className="block">
            <div className="box">
                <div className="columns">
                    <div className="column is-1">
                        <figure className="image">
                            <img className="is-rounded" src={`${reduxState.user.image?.imgProfile.base64}`} alt="" />
                        </figure>
                    </div>
                    <div className="column is-11">
                        <div className="field">
                            <div className="control">
                                <textarea className="textarea" placeholder="Votre message" rows={2} onChange={e => handleTexte(e.target.value)} maxLength={99} />
                            </div>
                            <p className="help">Il vous reste {Math.abs(textArticle.length - 100 + 1)} charactères</p>
                        </div>
                    </div>
                </div>
                <div className="columns">

                    <div className="column is-offset-1 is-5">
                        <UploadImage setSelectedImgProfil={setSelectedImgArticle} selectedImgProfil={selectedImgArticle} showImageSelected={true} />
                    </div>
                    <div className="column is-2">
                        {selectedImgArticle.name && <button className="button is-danger" onClick={() => removeSelectedImg()}>Supprimer image</button>}
                    </div>
                    <div className="column is-offset-1 is-3">
                        <button className={`button is-${textArticle || selectedImgArticle.name ? "primary" : "light"}`} onClick={() => (textArticle.length > 0 || selectedImgArticle.name) && sendPost()}>
                            Envoyer post
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AddPost
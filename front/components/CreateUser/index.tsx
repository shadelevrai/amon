import type { NextPage } from "next";
import { useEffect, useState, MouseEvent } from "react";
import Router from "next/router";
import * as EmailValidator from 'email-validator';

import { postAPI } from "../../libs/API/user"
import { uploadImageAPI } from "../../libs/API/image"
import putCookie from "../../libs/putCookie";
import upperCaseFirstLetter from "../../libs/uppercase";

import Img from "../../interfaces/Img";
import User from "../../interfaces/User"
import BirthDay from "../../interfaces/BirthDay"

import UploadImage from "../UploadImage";
import base64 from "../../playwright/integration/base64"
import Error from "../../interfaces/Error";

const CreateUse: NextPage = () => {

    const days: Array<number> = Array.from({ length: 31 }, (_, i) => i + 1)
    const months: Array<number> = Array.from({ length: 12 }, (_, i) => i + 1)
    const years: Array<number> = Array.from({ length: new Date().getFullYear() - 1900 }, (_, i) => i + 1901)
    const yearsReverse: Array<number> = years.reverse()

    const [email, setEmail] = useState<string>(process.env.NODE_ENV === "development" ? "aaaaaa@aaaaaa.fr" : "")
    const [password, setPassword] = useState<string>(process.env.NODE_ENV === "development" ? "aaaaaa" : "")
    const [samePassword, setSamePassword] = useState<string>(process.env.NODE_ENV === "development" ? "aaaaaa" : "")
    const [passwordSameError, setPasswordSameError] = useState<boolean>(false)
    const [lastName, setLastName] = useState<string>(process.env.NODE_ENV === "development" ? "Djoher" : "")
    const [firstName, setFirstName] = useState<string>(process.env.NODE_ENV === "development" ? "Ryan" : "")
    const [birthDay, setBirthDay] = useState<BirthDay>({
        day: process.env.NODE_ENV === "development" ? "01" : null,
        month: process.env.NODE_ENV === "development" ? "02" : null,
        year: process.env.NODE_ENV === "development" ? "1985" : null
    })
    const [selectedImgProfil, setSelectedImgProfil] = useState<Img>({
        name: process.env.NODE_ENV === "development" ? "avatar.jpg" : null,
        lastModified: process.env.NODE_ENV === "development" ? 1644081567249 : null,
        // webkitRelativePath: null,
        size: process.env.NODE_ENV === "development" ? 12200 : null,
        type: process.env.NODE_ENV === "development" ? "image/jpeg" : null,
        base64: process.env.NODE_ENV === "development" ? base64 : null
    })
    // const [imageProfileSizeError, setImageProfileSizeError] = useState<boolean>(false)
    const [emailTaken, setEmailTaken] = useState<boolean>(false)
    const [emailError, setEmailError] = useState<boolean>(false)



    function checkPassword(): void {
        if (samePassword.length > 0) {
            password != samePassword ? setPasswordSameError(true) : setPasswordSameError(false)
        }
    }

    function disabledButtonConfirmForm(): boolean {
        return email.length < 6 || password.length < 6 || samePassword.length < 6 || lastName.length < 3 || firstName.length < 3 || birthDay.day === null || birthDay.month === null || birthDay.year === null || !validateDate(`${birthDay.year}-${birthDay.month}-${birthDay.day}`) || selectedImgProfil.name === null || emailError === true ? false : true
    }

    function validateDate(date: string): boolean {
        return (new Date(date) !== "Invalid Date") && !isNaN(new Date(date));
    }

    function checkEmail(value: string): void {
        EmailValidator.validate(value) ? (setEmail(value), setEmailError(false)) : setEmailError(true)
    }

    async function confirmForm(e: MouseEvent): Promise<void> {
        e.preventDefault()
        try {
            const resImage: Img | Error = await uploadImageAPI(selectedImgProfil, "images/upload-image")
            if (!resImage.error) {
                const data = {
                    user: {
                        email,
                        password,
                        lastName: upperCaseFirstLetter(lastName),
                        firstName: upperCaseFirstLetter(firstName),
                        birthDay,
                        image: {
                            imgProfile: resImage._id
                        }
                    }
                }

                const resDateUser: User | Error = await postAPI(data, "/")
                resDateUser.error?.message === "Ce mail est déjà pris" ? setEmailTaken(true) : setEmailTaken(false);
                resDateUser.user && putCookie("user", resDateUser.user.token) && Router.push("/home")
            }
        } catch (err) {
            console.log("🚀 ~ file: index.tsx ~ line 93 ~ confirmForm ~ err", err)
        }
    }

    return (
        <div>
            <form>
                <label>
                    Email :
                    <div className="control">
                        <input className="input" type="email" id="email" value={`${email}`} placeholder="moi@gmail.com" onChange={e => checkEmail(e.target.value)} autoComplete="off" />
                    </div>
                </label>
                {emailError && <p>Le mail n'est pas le bon format</p>}
                <label>
                    Password :
                    <div className="control">
                        <input className="input" type="password" onChange={e => setPassword(e.target.value)} onBlur={() => checkPassword()} />
                    </div>
                </label>
                <label>
                    Retape Password :
                    <div className="control">
                        <input className="input" type="password" onChange={e => setSamePassword(e.target.value)} onBlur={() => checkPassword()} />
                    </div>
                </label>
                {passwordSameError && <p> erreur </p>}
                <label>
                    Nom :
                    <div className="control">
                        <input className="input" type="text" id="last_name" placeholder="Jean" onChange={e => setLastName(e.target.value)} autoComplete="off" />
                    </div>
                </label>
                <label>
                    Prénom :
                    <div className="control">
                        <input className="input" type="text" id="first_name" placeholder="Dupont" onChange={e => setFirstName(e.target.value)} autoComplete="off" />
                    </div>
                </label>
                <label>
                    Date de naissance :
                    <div className="control">
                        <div className="select">
                            <select id="birthday_day" onChange={e => setBirthDay(prevState => ({ ...prevState, day: parseInt(e.target.value) }))} autoComplete="off">
                                {days.map(day => (<option key={day}>{day}</option>))}
                            </select>
                        </div>
                        <div className="select">
                            <select id="birthday_month" onChange={e => setBirthDay(prevState => ({ ...prevState, month: parseInt(e.target.value) }))} autoComplete="off">
                                {months.map(month => (<option key={month}>{month}</option>))}
                            </select>
                        </div>
                        <div className="select">
                            <select id="birthday_year" onChange={e => setBirthDay(prevState => ({ ...prevState, year: parseInt(e.target.value) }))} autoComplete="off">
                                {yearsReverse.map(year => (<option key={year}>{year}</option>))}
                            </select>
                        </div>
                    </div>
                </label>
                <UploadImage setSelectedImgProfil={setSelectedImgProfil} selectedImgProfil={selectedImgProfil} showImageSelected={true} />
            </form>
            {disabledButtonConfirmForm() ? <button className="button" onClick={e => confirmForm(e)}>Inscription</button> : <button className="button" style={{ backgroundColor: "grey" }}>Inscription</button>}
            <button className="button" onClick={() => Router.push("/")}>Home</button>
            {emailTaken && <p>Cet email est déjà pris</p>}
        </div>
    )
}

export default CreateUse
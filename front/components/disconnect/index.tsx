import Cookies from 'universal-cookie';
import type { FunctionComponent } from "react";
import Router from 'next/router';
const cookies = new Cookies();

const Disconnect:FunctionComponent = () => {
    function removeCookie() {
        cookies.remove("user")
        sessionStorage.removeItem("imgCover")
        Router.push("/")
    }
    return (<div>
        <button className='button' onClick={() => removeCookie()}>Se déconnecter</button>
    </div>)
}

export default Disconnect
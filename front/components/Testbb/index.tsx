import { useEffect, useState } from "react"

const Testbb = () => {

    const [test, setTest] = useState({ lala: "lala", lolo: "lolo", lili: "lili" })

    function mama() {
        setTest({ lala: "", lolo: "", lili: "" })
    }

    useEffect(() => {
        console.log("🚀 ~ file: index.tsx ~ line 6 ~ Testbb ~ test", test)
    }, [test])

    return (
        <div onClick={() => mama()}>testbb</div>
    )
}

export default Testbb
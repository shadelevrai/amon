import { useRouter } from "next/router";
import type { FunctionComponent } from "react";

import { postAPI } from "../../libs/API/user";
import putCookie from "../../libs/putCookie";

import User from "../../interfaces/User";

interface Step {
    step: "create" | "login"
}

const CreateAndLogin: FunctionComponent<Step> = ({ step }) => {

    const router = useRouter();

    interface Form {
        email: string,
        password: string

    }
    const form: Form = {
        email: "aaaaaa@aaaaaa.fr",
        password: "aaaaaa"
    };

    function checkUserData(response: User): boolean {
        const { user, errors, errorServer } = response;
        if (user?._id) {
            return true;
        }
        if (errors) {
            console.error(errors);
            return false;
        }
        if (errorServer) {
            console.error(errorServer);
            return false;
        }
        return false;
    }

    function handleInput(e: any): void {
        form[e.target.name] = e.target.value
    }

    async function confirmForm(e: any): Promise<void> {
        try {
            e.preventDefault()
            const data = {
                user: form,
            };
            const response: any = step === "create" ? await postAPI(data, "/") : await postAPI(data, "/login")
            checkUserData(response) && putCookie("user", response.user.token) && router.push("/home")
        } catch (error) {
            console.log("🚀 ~ file: index.tsx ~ line 80 ~ confirmForm ~ error", error)
        }
    }

    return (
        <form>
            <div className="field">
                <label className="label">Email
                    <div className="control mt-1">
                        <input className="input" defaultValue="" type="text" name="email" onChange={e => handleInput(e)} />
                    </div>
                </label>
            </div>
            <div className="field">
                <label className="label"> Password
                    <div className="control mt-1">
                        <input className="input" type="password" name="password" onChange={e => handleInput(e)} />
                    </div>
                </label>
                <div className="level">
                    <div className="level-left"></div>
                    <div className="level-right">
                        <div className="level-item">
                            <input className="button mt-4" type="submit" value={step} onClick={e => confirmForm(e)} />
                        </div>
                    </div>
                </div>
            </div>
        </form>

    )
}

export default CreateAndLogin
import type { FunctionComponent } from "react";

interface SubmitButtonInterface {
  text:string,
}

const SubmitButton: FunctionComponent<SubmitButtonInterface> = ({ text }) => {
  return (
    <div>
      <button>{text}</button>
    </div>
  );
};

export default SubmitButton;

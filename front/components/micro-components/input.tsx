import type { FunctionComponent } from "react";
import { useSelector, useDispatch } from "react-redux";

interface InputParametersInterface {
  text: string;
  type: string;
  storeActionType: string;
  name: string
}

const Input: FunctionComponent<InputParametersInterface> = ({ text, type, storeActionType, name }) => {
  const dispatch = useDispatch();
  // const reduxState = useSelector((state) => state);

  function handleValue(value: string): void {
    dispatch({ type: storeActionType, step: value });
  }

  return (
    <div>
      <label htmlFor={name}>{text}</label>
       : <input type={type} onChange={(e) => handleValue(e.target.value)} name={name} id={name} />
    </div>
  );
};

export default Input;

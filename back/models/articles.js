const mongoose = require('mongoose');

const { Schema } = mongoose;

const ArticlesSchema = new Schema({
    date: Date,
    text:String,
})

ArticlesSchema.methods.toAuthJSON=function(){
    return {
        _id:this._id,
        date:this.date,
        text:this.text
    }
}

mongoose.model("Articles", ArticlesSchema)
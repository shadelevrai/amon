const mongoose = require('mongoose');

const { Schema } = mongoose;

const ImagesSchema = new Schema({
    name: String,
    lastModified: String,
    size: Number,
    type: String,
    base64: String
})

ImagesSchema.methods.toAuthJSON = function () {
    return {
        _id:this._id,
        name: this.name,
        lastModified:this.lastModified,
        size:this.size,
        type:this.type,
        base64:this.base64
    }
}

mongoose.model("Images", ImagesSchema)
require('dotenv').config()

const express = require('express');
const app = express();
const cors = require("cors")
const path = require('path');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const session = require('express-session');

mongoose.promise = global.Promise;

app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'passport',
    cookie: {
        maxAge: 60000
    },
    resave: false,
    saveUninitialized: false
}));
app.use(cors())

mongoose.connect(process.env.MONGO_CONNECT, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
});
mongoose.set('debug', true);

require('./models/Users');
require('./models/Images');
require('./models/Articles');

require('./config/passport');

app.use(require('./routes'))

app.listen(process.env.PORT, () => console.log(`Server running on http://localhost:${process.env.PORT}/`));
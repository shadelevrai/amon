
const express = require('express');
const router = express.Router();

router.use('/users', require('./users'));
router.use('/images',require('./images'))
router.use('/articles',require('./articles'))


module.exports = router;
const mongoose = require("mongoose");
const passport = require("passport");
const router = require("express").Router();
const multer = require('multer');
const auth = require("../auth");
const Users = mongoose.model("Users");
const fs = require("fs-extra");
const mailer = require("../../libs/nodemailer");

//POST new user route (optional, everyone has access)

router.post("/", auth.optional, (req, res, next) => {
  const {
    body: { user },
  } = req;

  if (!user.email || !user.password || !user.lastName || !user.firstName || !user.birthDay.day || !user.birthDay.month || !user.birthDay.year || !user.image.imgProfile) {
    return res.status(422).json({
      error: {
        message: "Un champ n'a pas été envoyé"
      }
    })
  }

  return Users.find({ email: user.email }).then(response => {
    if (response.length) {
      return res.status(422).json({
        error: {
          message: "Ce mail est déjà pris"
        }
      })
    }
    const finalUser = new Users(user);

    finalUser.setPassword(user.password);

    return finalUser.save().then(() => res.json({ user: finalUser.toAuthJSON() }));
  })

});

//POST login route (optional, everyone has access)
router.post("/login", auth.optional, (req, res, next) => {
  const {
    body: { user },
  } = req;

  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: "is required",
      },
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: "is required",
      },
    });
  }

  return passport.authenticate("local", { session: false }, (err, passportUser, info) => {
    if (err) {
      // return next(err);
      return res.status(422).json({
        errors: {
          message: "Wrong log",
        },
      });
    }

    if (passportUser) {
      const user = passportUser;
      user.token = passportUser.generateJWT();

      return res.json({ user: user.toAuthJSON() });
    }

    return status(400).info;
  })(req, res, next);
});

//GET current route (required, only authenticated users have access)
router.get("/current", auth.required, (req, res, next) => {
  if (!req.payload.id) {
    return res.status(422).json({
      error: {
        message: "Le id du payload n'a pas été reçu par le serveur"
      }
    })
  }
  const {
    payload: { id },
  } = req;

  return Users.findById(id).then((user) => {
    if (!user) {
      return res.status(400).res.json({
        error: {
          message: "L'utilisateur n'a pas été trouvé"
        }
      });
    }
    return res.json({ user: user.toAuthJSON() });
  });
});

router.patch("/modify", auth.required, (req, res, next) => {
  const {
    payload: { id },
  } = req;

  const {
    body: { user },
  } = req;

  return Users.findByIdAndUpdate(id, { [user.email]: user.email }, { new: true }).then((user) => {
    if (!user) {
      return res.sendStatus(400);
    }
    return res.json({ user: user.toAuthJSON() })
  });
});

router.patch("/add-new-image-cover-for-user", auth.required, (req, res) => {

  if (!req.body.img_id || !req.payload.id) {
    return res.status(422).json({
      error: {
        message: "id du payload ou id de l'image non reçu par le serveur"
      }
    })
  }

  const {
    payload: { id },
  } = req;

  const {
    body: { img_id },
  } = req;

  return Users.findByIdAndUpdate(id,
    { $set: { "image.imgCover": img_id } },
    { new: true, upsert: true }).then(result => {
      if (!result) {
        return res.status(400).json({
          error: {
            message: "Le serveur n'a pas trouvé d'image correspondant à l'id donné"
          }
        });
      }
      return res.json({ user: result.toAuthJSON() })
    })

})

router.patch("/remove-image-cover-for-user", auth.required, (req, res) => {
  if (!req.payload.id) {
    return res.status(422).json({
      error: {
        message: "id du payload ou id de l'image non reçu par le serveur"
      }
    })
  }
  const {
    payload: { id },
  } = req;

  return Users.findByIdAndUpdate(id, { $unset: { "image.imgCover": "" } }, { new: true, upsert: true }).then(result => {
    return res.json({ user: result.toAuthJSON() })
  })
})

router.patch("/add-element-array", auth.required, (req, res) => {
  if (!req.body.elem || !req.payload.id) {
    return res.status(422).json({
      error: {
        message: "id du payload ou id de l'article non reçu par le serveur"
      }
    })
  }

  const {
    payload: { id },
  } = req;

  const {
    body: { elem },
  } = req;

  return Users.findByIdAndUpdate(id,
    { $push: { articles: elem } },
    { new: true, upsert: true }).then(result => {
      if (!result) {
        return res.status(400).json({
          error: {
            message: "Le serveur n'a pas trouvé d'image correspondant à l'id donné"
          }
        });
      }
      return res.json({ user: result.toAuthJSON() })
    })

})

router.delete("/delete", auth.required, (req, res, next) => {
  const {
    payload: { id },
  } = req;
  return Users.findByIdAndRemove(id).then(user => {
    if (!user) {
      return res.sendStatus(400);
    }
    return res.json({ message: "User deleted" })
  })
})

router.get("/mail-exist/:mail", auth.optional, (req, res) => {
  const { mail } = req.params
  return Users.findOne({ email: mail }).then(user => {
    if (user) {
      const resa = mailer(mail)
      if (resa) {
        return res.status(200).json({ message: true })
      }
      return res.status(400).json({ message: false })
    }
    return res.status(400).json({ message: false })
  })
})

module.exports = router;

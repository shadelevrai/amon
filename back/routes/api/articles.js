const mongoose = require("mongoose");
const router = require("express").Router();
const auth = require("../auth");
const Articles = mongoose.model("Articles")

router.post("/post-article", auth.required, (req, res) => {
    if (!req.body.article) {
        return res.status(422).json({
            error: {
                message: "Le texte de l'article n'a pas été reçu par le serveur"
            }
        })
    }
    const { body: { article } } = req
    article.date = new Date();
    console.log("🚀 ~ file: articles.js ~ line 16 ~ router.post ~ article.date", article.date)
    const finalArticle = new Articles(article)
    return finalArticle.save().then(() => res.json({ article: finalArticle.toAuthJSON() }));
})

router.get("/:id_art", auth.required, (req, res) => {
    // const {
    //     payload: { id },
    // } = req;

    const { params: { id_art } } = req

    return Articles.findById(id_art).then(article => {
        if (!article) {
            return res.status(400).res.json({
                error: {
                    message: "L'article n'a pas été trouvé"
                }
            });
        }
        return res.json({ article: article.toAuthJSON() })
    })

})

module.exports = router;
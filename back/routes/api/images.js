const mongoose = require("mongoose");
const router = require("express").Router();
const auth = require("../auth");
const Images = mongoose.model("Images")

router.get("/:id", auth.required, (req, res) => {
    if (!req.params.id) {
        return res.status(422).json({
            error: {
                message: "Le champ de l'id de l'image n'a pas été reçu par le serveur"
            }
        })
    }
    return Images.findById(req.params.id)
        .then(result => {
            if (!result) {
                return res.status(400).json({
                    error: {
                        message: "Le serveur n'a pas trouvé d'image correspondant à l'id donné"
                    }
                });
            }
            return res.json({ image: result.toAuthJSON() })
        })
})

router.post("/upload-image", auth.optional, (req, res) => {
    if (!req.body.imgWihtoutBase64 || !req.body.base64) {
        return res.status(422).json({
            error: {
                message: "Un champ n'a pas été envoyé"
            }
        })
    }
    const {
        body: { imgWihtoutBase64, base64 },
    } = req;
    const { name, lastModified, size, type } = imgWihtoutBase64
    const image = { base64, name, lastModified, size, type }

    const finalImage = new Images(image)
    return finalImage.save().then(() => res.json({ image: finalImage.toAuthJSON() }));
})

router.delete("/delete-image/:id", auth.required, (req, res) => {
    if (!req.params.id) {
        return res.status(422).json({
            error: {
                message: "Le champ de l'id de l'image ou l'id du payload n'a pas été reçu par le serveur"
            }
        })
    }
    return Images.findByIdAndDelete(req.params.id).then(result => {
        if (result) {
            return res.json({ message: "images supprimée" })
        } else {
            return res.json({ error: { message: "l'image existe pas" } })
        }
    })
})

module.exports = router;